/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author thais
 */
@Entity
@Table(name = "tb_departamento_funcionario")
public class DepartamentoFuncionario implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String ID = "defu";
    public static final String SEQ = "seq_defu";

    @Id
    @Column(name = ID, nullable = false, unique = true)
    @SequenceGenerator(sequenceName = SEQ + ID, name = SEQ + ID, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @JoinColumns({ @JoinColumn(name = "func_matricula", referencedColumnName = "func_matricula") })
    @OneToMany
    private Funcionario funcionario;

    @JoinColumns({ @JoinColumn(name = "depa_id", referencedColumnName = "depa_id") })
    @OneToMany
    private Departamento departamento;

    public DepartamentoFuncionario() {
    }

    public DepartamentoFuncionario(Long id, Funcionario funcionario, Departamento departamento) {
        this.id = id;
        this.funcionario = funcionario;
        this.departamento = departamento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

}
