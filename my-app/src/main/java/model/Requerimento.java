/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 *
 * @author thais
 */
@Entity
@Table(name = "tb_requerimento")
public class Requerimento implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String ID = "requ_id";
    public static final String SEQ = "seq_requ";

    @Id
    @Column(name = ID, nullable = false, unique = true)
    @SequenceGenerator(sequenceName = SEQ + ID, name = SEQ + ID, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "requ_descricao")
    private String descricao;

    @Column(name = "requ_data")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate data;

    @JoinColumns({ @JoinColumn(name = "anexo_id", referencedColumnName = "anexo_id") })
    @OneToMany
    private Anexo anexo;

    @JoinColumns({ @JoinColumn(name = "tire_id", referencedColumnName = "tire_id") })
    @OneToMany
    private TipoRequerimento tipoRequerimento;

    public Requerimento(Long id, String descricao, LocalDate data, Anexo anexo, TipoRequerimento tipoRequerimento) {
        this.id = id;
        this.descricao = descricao;
        this.data = data;
        this.anexo = anexo;
        this.tipoRequerimento = tipoRequerimento;
    }

    public Requerimento() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Anexo getAnexo() {
        return anexo;
    }

    public void setAnexo(Anexo anexo) {
        this.anexo = anexo;
    }

    public TipoRequerimento getTipoRequerimento() {
        return tipoRequerimento;
    }

    public void setTipoRequerimento(TipoRequerimento tipoRequerimento) {
        this.tipoRequerimento = tipoRequerimento;
    }

}
