/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conexaoBD.ConnectionFactory;
import model.Usuario;

/**
 *
 * @author thais
 */
public class UsuarioDAO {

    private Connection con = null;

    public UsuarioDAO() throws SQLException {
        con = ConnectionFactory.getConnection();
    }

    public PreparedStatement insert(Usuario user) {
        String sql = "INSERT INTO tb_usuario(usua_id, usua_nome, usua_genero, usua_rg, usua_cpf, usua_login, usua_senha, ativo, tius_id)"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setLong(1, user.getId());
            stmt.setString(2, user.getNome());
            stmt.setString(3, user.getGenero());
            stmt.setInt(4, user.getRg());
            stmt.setInt(5, user.getCpf());
            stmt.setString(6, user.getLogin());
            stmt.setString(7, user.getSenha());
            stmt.setLong(8, user.getAtivo());
            stmt.setLong(9, user.getTipoUsuario().getId());
            stmt.executeUpdate();

            return stmt;// se n�o der trocar para boolean = true

        } catch (SQLException e) {
            System.out.println("N�o foi possivel inserir Usuario no banco" + e.getMessage());
        }

        return null;
    }

    public PreparedStatement update(Usuario user) {
        String sql = "UPDATE tb_usuario set usua_nome = ?, usua_genero = ?, usua_rg = ?, usua_cpf = ?, usua_login = ?, usua_senha = ?, ativo = ?, tius_id = ?"
                + "where usua_id = " + user.getId();

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, user.getNome());
            stmt.setString(2, user.getGenero());
            stmt.setInt(3, user.getRg());
            stmt.setInt(4, user.getCpf());
            stmt.setString(5, user.getLogin());
            stmt.setString(6, user.getSenha());
            stmt.setLong(7, user.getAtivo());
            stmt.setLong(8, user.getTipoUsuario().getId());
            stmt.executeUpdate();

            return stmt;

        } catch (SQLException e) {
            System.out.println("N�o foi possivel inserir Usuario no banco" + e.getMessage());
        }

        return null;
    }

    public Boolean desativaAluno(Usuario user) {
        String sql = "UPDATE tb_usuario set ativo = 'f' where usua_id = " + user.getId();

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();

            return true;

        } catch (SQLException e) {
            System.out.println("N�o foi possivel mudar o Ativo do Usuario" + e.getMessage());
        }

        return null;
    }

    public List<Usuario> loadAll() {
        String sql = "SELECT * FROM tb_usuario";

        PreparedStatement stmt = null;
        ResultSet retorno = null;

        List<Usuario> listaUsuarios = new ArrayList<>();

        try {
            stmt = con.prepareStatement(sql);
            retorno = stmt.executeQuery();

            while (retorno.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(retorno.getLong("usua_id"));
                usuario.setNome(retorno.getString("usua_nome"));
                usuario.setGenero(retorno.getString("usua_genero"));
                usuario.setRg(retorno.getInt("usua_rg"));
                usuario.setCpf(retorno.getInt("usua_genero"));
                usuario.setLogin(retorno.getString("usua_login"));
                usuario.setSenha(retorno.getString("senha"));
                usuario.setAtivo((char) retorno.getLong("ativo"));
                usuario.getTipoUsuario().setId((retorno.getLong("tius_id")));
                listaUsuarios.add(usuario);
            }

        } catch (SQLException e) {
            System.out.println("Nao foi possivel buscar os usuarios no banco" + e.getMessage());

        } finally {
            ConnectionFactory.fecharConexao(con, stmt);
        }

        return listaUsuarios;
    }

    public List<Usuario> loadById(Usuario user) {
        String sql = "SELECT * FROM tb_usuario where usua_id =" + user.getId();

        PreparedStatement stmt = null;
        ResultSet retorno = null;

        List<Usuario> listaUsuarios = new ArrayList<>();

        try {
            stmt = con.prepareStatement(sql);
            retorno = stmt.executeQuery();

            while (retorno.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(retorno.getLong("usua_id"));
                usuario.setNome(retorno.getString("usua_nome"));
                usuario.setGenero(retorno.getString("usua_genero"));
                usuario.setRg(retorno.getInt("usua_rg"));
                usuario.setCpf(retorno.getInt("usua_genero"));
                usuario.setLogin(retorno.getString("usua_login"));
                usuario.setSenha(retorno.getString("senha"));
                usuario.setAtivo((char) retorno.getLong("ativo"));
                usuario.getTipoUsuario().setId((retorno.getLong("tius_id")));
                listaUsuarios.add(usuario);
            }

        } catch (SQLException e) {
            System.out.println("Nao foi possivel buscar o usuario por id no banco" + e.getMessage());

        } finally {
            ConnectionFactory.fecharConexao(con, stmt);
        }

        return listaUsuarios;
    }
}
