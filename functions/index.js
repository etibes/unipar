
const functions = require('firebase-functions');
const admin = require("firebase-admin");


var serviceAccount = require("./permission.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://secretaria-online-b1f41.firebaseio.com"
});


const db = admin.firestore();
const app = require("express")();

const cors = require("cors");
app.use(cors({
    origin: true
}));

app.get("/loadById/:id", function (req, res) {
    (async () => {
        try {
            const document = db.collection('requerimento').doc(req.params.id);
            let requerimento = await document.get();
            let response = requerimento.data();

            return res.status(200).send(response);
        } catch (error) {
            console.log(error);

            return res.status(500).send(error);

        }
    })();
});

//post
app.post("/create", function (req, res) {
    (async () => {
        try {
            await db.collection('requerimento').doc('/' + req.body.id + '/')
                .create({
                    id: req.body.id,
                    descricao: req.body.descricao
                })

            return res.status(200).send();
        } catch (error) {
            console.log(error);

            return response.status(500).send(error);

        }
    })();
});

app.get("/loadAll", function (req, res) {
    (async () => {
        try {
            let query = db.collection('requerimento');
            let response = [];

            await query.get().then(querySnapshot => {
                let docs = querySnapshot.docs;//resultado da query

                for (let doc of docs) {
                    const selectedItem = {
                        id: doc.id,
                        descricao: doc.data().descricao
                    };

                    response.push(selectedItem);
                }

                return response;//deve retornar um valor
            })

            return res.status(200).send(response);

        } catch (error) {
            console.log(error);

            return res.status(500).send(error);

        }
    })();
});

const runtimeOpts = { timeoutSeconds: 540, memory: '1GB' }

exports.myStorageFunction = functions
    .runWith(runtimeOpts)
    .storage
    .object()
    .onFinalize((object) => {
    });

exports.api = functions.https.onRequest(app);


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
