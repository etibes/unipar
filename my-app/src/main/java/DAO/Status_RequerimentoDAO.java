/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import conexãoDB.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Status_Requerimento;

/**
 *
 * @author alex
 */
public class Status_RequerimentoDAO {
    private Connection con = null;
    
    public Status_RequerimentoDAO() {
        con = ConnectionFactory.getConexao();
    }
    
    public boolean inserir_status_requerimento(Status_Requerimento status_Requerimento){
        
        String sql = "INSERT INTO status_requerimento (descricao, dtCadastro, dtAlteracao) "
                + "VALUES (?, ?, ?)";
        
        PreparedStatement stmt = null;
        
        try{
            stmt = con.prepareStatement(sql);
            stmt.setString(1, status_Requerimento.getDescricao());
            stmt.setDate(2, status_Requerimento.getDtCadastro());
            stmt.setDate(3, status_Requerimento.getDtAlteracao());
            stmt.executeUpdate();
            
            return true;
            
        }catch(SQLException ex){
            System.out.println("Erro ao inserir o status_requerimento: "+ex.getMessage());
            return false;
        }
        
    }
    
    public List<Status_Requerimento> buscar_status_requerimento(){
        
        String sql = "SELECT * FROM status_requerimento";
        
        PreparedStatement stmt = null;
        ResultSet retorno = null;
        
        List<Status_Requerimento> listaStatus_requerimento = new ArrayList<>();
        
        try{
            stmt = con.prepareStatement(sql);
            retorno = stmt.executeQuery();
            
            while(retorno.next()){
                Status_Requerimento status_Requerimento = new Status_Requerimento();
                status_Requerimento.setId_Status(retorno.getBigDecimal("id_Status"));           //Verificar como ficara e Transformar em BigInterger
                
                status_Requerimento.setDescricao(retorno.getString("descricao"));
                status_Requerimento.setDtCadastro(retorno.getString("dtCadastro"));             //Verificar como ficara
                status_Requerimento.setDtAlteracao(retorno.getString("dtAlteracao"));
                listaStatus_requerimento.add(status_Requerimento);
                
            }
            
        }catch(SQLException ex){
            System.out.println("Erro ao buscar status_Requerimento: "+ex.getMessage());
        }finally{
            ConnectionFactory.fecharConexao(con, stmt);
        }
        return listaStatus_requerimento;
        
    }
    
    public List<Status_Requerimento> deleta_status_requerimento(){   // terminar
        
        String sql = "DELETE * FROM status_requerimento";            //corrigir
        
        PreparedStatement stmt = null;
        ResultSet retorno = null;
        
        List<Status_Requerimento> listaStatus_requerimento = new ArrayList<>();
        
        try{
            stmt = con.prepareStatement(sql);
            retorno = stmt.executeQuery();
            
            while(retorno.next()){
                Status_Requerimento status_Requerimento = new Status_Requerimento();
                status_Requerimento.setId_Status(retorno.getBigDecimal("id_Status"));           //Verificar como ficara e Transformar em BigInterger
                
                status_Requerimento.setDescricao(retorno.getString("descricao"));
                status_Requerimento.setDtCadastro(retorno.getString("dtCadastro"));             //Verificar como ficara
                status_Requerimento.setDtAlteracao(retorno.getString("dtAlteracao"));
                listaStatus_requerimento.add(status_Requerimento);
                
            }
            
        }catch(SQLException ex){
            System.out.println("Erro ao deletar status_requerimento: "+ex.getMessage());
        }finally{
            ConnectionFactory.fecharConexao(con, stmt);
        }
        return listaStatus_requerimento;
        
    }
}
