/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author thais
 */
@Entity
@Table(name = "tb_disciplina_curso")
public class DisciplinaCurso implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String ID = "dicu_id";
    public static final String SEQ = "seq_dicu";

    @Id
    @Column(name = ID, nullable = false, unique = true)
    @SequenceGenerator(sequenceName = SEQ + ID, name = SEQ + ID, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @JoinColumns({ @JoinColumn(name = "disc_id", referencedColumnName = "disc_id") })
    @OneToMany
    private Disciplina disciplina;

    @JoinColumns({ @JoinColumn(name = "curso_id", referencedColumnName = "curso_id") })
    @OneToMany
    private Curso curso;

    public DisciplinaCurso() {
    }

    public DisciplinaCurso(Long id, Disciplina disciplina, Curso curso) {
        this.id = id;
        this.disciplina = disciplina;
        this.curso = curso;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

}
