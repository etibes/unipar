/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexaoBD;

import java.sql.*;
import java.sql.DriverManager;

/**
 *
 * @author thais
 */
public class ConnectionFactory {

    private static final String DRIVE = "org.postgresql.Driver.class";
    private static final String URL = "jdbc:postgres://localhost:3306/baseTeste";
    private static final String USER = "postgres";
    private static final String SENHA = "sucesso";

    public static Connection getConnection () throws SQLException {
        try {
            Class.forName(DRIVE);
            return DriverManager.getConnection(URL, USER, SENHA);
        } catch (ClassNotFoundException ex) {
            System.out.println("cagoo na conexao"+ex.getMessage());

        }
        return null;
    }

    public static void fecharConexao (Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println("cagoo na horaa de fechar conexao"+e.getMessage());
            }
        }
    }

    public static void fecharConexao (Connection con, PreparedStatement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                System.out.println("cagoo na horaa de fechar conexao"+e.getMessage());
            }
        }

        fecharConexao(con);
    }
}
