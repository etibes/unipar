package core;

/**
 *
 * @author thais
 */
public class MensagemTela {
	private String mensagem;
    private String categoria;
    private String seletorFocus;
	private String width;
	private String height;


	public MensagemTela(String msg, String e) {
		this.mensagem = msg;
		this.categoria = e;
    }

    public MensagemTela(String msg, String e, String seletorFocus) {
		this.mensagem = msg;
        this.categoria = e;
        this.seletorFocus = seletorFocus;
    }

	public MensagemTela(String msg, String e, String height, String width) {
		this.mensagem = msg;
        this.categoria = e;
        this.height = height;
        this.width = width;
    }

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String tipoMsg) {
		this.categoria = tipoMsg;
    }

    public String getSeletorFocus() {
        return this.seletorFocus;
    }

    public void setSeletorFocus(String seletorFocus) {
        this.seletorFocus = seletorFocus;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
