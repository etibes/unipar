package core.Exception;

import java.util.LinkedList;
import java.util.List;

import br.com.caelum.vraptor.validator.Message;
import core.IMensagem;
import core.MensagemTela;

/**
 *
 * @author thais
 */
public class Mensagem extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private MensagemTela mensagem;
    private List<MensagemTela> mensagens = null;

    public Mensagem(String msg) {
        super(msg);
        mensagem = new MensagemTela(msg, CategoriaMensagem.ERRO.getTipo());
    }

    public Mensagem(String msg, CategoriaMensagem tipoMsg) {
        super(msg);
        mensagem = new MensagemTela(msg, tipoMsg.getTipo());
    }

    public Mensagem(String msg, CategoriaMensagem tipoMsg, String height, String width) {
        super(msg);
        mensagem = new MensagemTela(msg, tipoMsg.getTipo(), height, width);
    }

    public Mensagem(String msg, String field) {
        super(msg);
        mensagem = new MensagemTela(msg, CategoriaMensagem.ERRO.getTipo(), field);
    }

    @Deprecated
    public Mensagem(List<Message> criticas) {
        mensagens = new LinkedList<>();

        for (Message m : criticas) {
            mensagens.add(new MensagemTela(m.getMessage(), CategoriaMensagem.ERRO.getTipo()));
        }
    }

    public Mensagem(List<String> messageList, CategoriaMensagem e) {
        mensagens = new LinkedList<>();

        for (String m : messageList) {
            mensagens.add(new MensagemTela(m, e.getTipo()));
        }
    }

    /**
     * @param ism
     */
    public Mensagem(IMensagem ism) {
        this(ism.getMensagem());
    }

    public MensagemTela getMensagem() {
        return mensagem;
    }

    public void setMensagem(MensagemTela mensagem) {
		this.mensagem = mensagem;
	}

	public List<MensagemTela> getMensagens() {
		return mensagens;
	}

    public void setMensagem(List<MensagemTela> mensagens) {
        this.mensagens = mensagens;
    }

    public Object getMensagensErro() {
        if (this.getMensagem() != null) {
            return this.getMensagem();
        }
        if (this.getMensagem() != null) {
            return this.getMensagem();
        } else
            return new MensagemTela("N�o foi poss�vel completar a informa��o. A mensagem de retorno cont�m problemas. "
                    + "Por favor, informe este problema ao nosso suporte t�cnico ou ao administrador do sistema.",
                    CategoriaMensagem.ERRO.getTipo());
    }

}