package core;

public interface IMensagem {

    public String getCodigo();

    public String getMensagem();

    public ETipoMensagens getTipo();

    /**
     * @return
     */
    public default String getMensagemCompleta() {
        return getCodigo() + " - " + getMensagem();
    };
}