/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import conexãoDB.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Status;

/**
 *
 * @author alex
 */
public class StatusDAO {
    private Connection con = null;
    
    public StatusDAO() {
        con = ConnectionFactory.getConexao();
    }
    
    public boolean inserir_Status(Status status){
        
        String sql = "INSERT INTO status (descricao) "
                + "VALUES (?)";
        
        PreparedStatement stmt = null;
        
        try{
            stmt = con.prepareStatement(sql);
            stmt.setString(1, status.getDescricao());
            stmt.executeUpdate();
            
            return true;
            
        }catch(SQLException ex){
            System.out.println("Erro ao inserir o status: "+ex.getMessage());
            return false;
        }
        
    }
    
    public List<Status> buscar_Status(){
        
        String sql = "SELECT * FROM status";
        
        PreparedStatement stmt = null;
        ResultSet retorno = null;
        
        List<Status> listaStatus = new ArrayList<>();
        
        try{
            stmt = con.prepareStatement(sql);
            retorno = stmt.executeQuery();
            
            while(retorno.next()){
                Status status = new Status();
                status.setId_Status(retorno.getBigDecimal("id_Status"));           //Transformar em BigInterger
                status.setDescricao(retorno.getString("descricao"));
                listaStatus.add(status);
                
            }
            
        }catch(SQLException ex){
            System.out.println("Erro ao buscar status: "+ex.getMessage());
        }finally{
            ConnectionFactory.fecharConexao(con, stmt);
        }
        return listaStatus;
        
    }
    
    public List<Status> deleta_Status(){   // terminar
        
        String sql = "DELETE * FROM status";    //Corrigir
        
        PreparedStatement stmt = null;
        ResultSet retorno = null;
        
        List<Status> listaStatus = new ArrayList<>();
        
        try{
            stmt = con.prepareStatement(sql);
            retorno = stmt.executeQuery();
            
            while(retorno.next()){
                Status status = new Status();
                status.setId_Status(retorno.getBigDecimal("id_Status"));           //Transformar em BigInterger
                status.setDescricao(retorno.getString("descricao"));
                listaStatus.add(status);
                
            }
            
        }catch(SQLException ex){
            System.out.println("Erro ao deletar status: "+ex.getMessage());
        }finally{
            ConnectionFactory.fecharConexao(con, stmt);
        }
        return listaStatus;
        
    }
}
