/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author thais
 */
@Entity
@Table(name = "tb_cidade")
public class Cidade implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String ID = "cida_id";
    public static final String SEQ = "seq_cida";

    @Id
    @Column(name = ID, nullable = false, unique = true)
    @SequenceGenerator(sequenceName = SEQ + ID, name = SEQ + ID, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "cida_nome")
    private String nome;

    @Column(name = "cida_uf")
    private String uf;

    public Cidade() {
    }

    public Cidade(Long id, String nome, String uf) {
        this.id = id;
        this.nome = nome;
        this.uf = uf;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

}
