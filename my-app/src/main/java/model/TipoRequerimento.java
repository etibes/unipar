/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author thais
 */
@Entity
@Table(name = "tb_tipo_requerimento")
public class TipoRequerimento implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String ID = "tire_id";
    public static final String SEQ = "seq_tire";

    @Id
    @Column(name = ID, nullable = false, unique = true)
    @SequenceGenerator(sequenceName = SEQ + ID, name = SEQ + ID, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "tire_descricao")
    private String descricao;

    @Column(name = "tire_requisito")
    private String requisito;

    @JoinColumns({ @JoinColumn(name = "orre_id", referencedColumnName = "orre_id") })
    @OneToMany
    private OrdemRequerimento ordemRequerimento;

    public TipoRequerimento(Long id, String descricao, String requisito, OrdemRequerimento ordemRequerimento) {
        this.id = id;
        this.descricao = descricao;
        this.requisito = requisito;
        this.ordemRequerimento = ordemRequerimento;
    }

    public TipoRequerimento() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getRequisito() {
        return requisito;
    }

    public void setRequisito(String requisito) {
        this.requisito = requisito;
    }

    public OrdemRequerimento getOrdemRequerimento() {
        return ordemRequerimento;
    }

    public void setOrdemRequerimento(OrdemRequerimento ordemRequerimento) {
        this.ordemRequerimento = ordemRequerimento;
    }

}
