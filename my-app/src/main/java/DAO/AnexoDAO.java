/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import conexãoDB.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.Anexo;

/**
 *
 * @author alex
 */
public class AnexoDAO {
    
    private Connection conn;
    
    public AnexoDAO() {
        this.conn = ConnectionFactory.getConexao();
    }
    
    public Boolean inserir(Anexo anexo) throws SQLException{
        Boolean retorno = false;
        String sql = "INSERT INTO anexo (documento) VALUES (?)";
        
        PreparedStatement stmt = conn.prepareStatement(sql);
        try{
            stmt.setBytes(1, anexo.getDocumento());
            retorno = stmt.execute();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
        return retorno;
    }
}
