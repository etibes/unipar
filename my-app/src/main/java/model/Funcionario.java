/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author thais
 */
@Entity
@Table(name = "tb_funcionario")
public class Funcionario implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String ID = "func_matricula";
    public static final String SEQ = "seq_func";

    @Id
    @Column(name = ID, nullable = false, unique = true)
    @SequenceGenerator(sequenceName = SEQ + ID, name = SEQ + ID, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @JoinColumns({ @JoinColumn(name = "usua_id", referencedColumnName = "usua_id") })
    @OneToMany
    private Usuario usuario;

    public Funcionario(Long id, Usuario usuario) {
        this.id = id;
        this.usuario = usuario;
    }

    public Funcionario() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

}
