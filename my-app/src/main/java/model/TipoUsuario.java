/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author thais
 */
public class TipoUsuario implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String descricao;

    public TipoUsuario(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public TipoUsuario() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
