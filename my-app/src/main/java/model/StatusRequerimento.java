/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 *
 * @author thais
 */
@Entity
@Table(name = "tb_status_requerimento")
public class StatusRequerimento implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String ID = "stareq_id";
    public static final String SEQ = "seq_stareq";

    @Id
    @Column(name = ID, nullable = false, unique = true)
    @SequenceGenerator(sequenceName = SEQ + ID, name = SEQ + ID, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "stareq_descricao")
    private String descricao;

    @Column(name = "stareq_descricao")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private ZonedDateTime dataCadastro;

    @Column(name = "stareq_descricao")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private ZonedDateTime dataAlteracao;

    @JoinColumns({ @JoinColumn(name = "status_id", referencedColumnName = "status_id") })
    @OneToMany
    private Status status;

    @JoinColumns({ @JoinColumn(name = "requ_id", referencedColumnName = "requ_id") })
    @OneToMany
    private Requerimento requerimento;

    @JoinColumns({ @JoinColumn(name = "func_matricula", referencedColumnName = "func_matricula") })
    @OneToMany
    private Funcionario funcionario;

    public StatusRequerimento(Long id, String descricao, ZonedDateTime dataCadastro, ZonedDateTime dataAlteracao,
            Status status, Requerimento requerimento, Funcionario funcionario) {
        this.id = id;
        this.descricao = descricao;
        this.dataCadastro = dataCadastro;
        this.dataAlteracao = dataAlteracao;
        this.status = status;
        this.requerimento = requerimento;
        this.funcionario = funcionario;
    }

    public StatusRequerimento() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public ZonedDateTime getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(ZonedDateTime dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public ZonedDateTime getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(ZonedDateTime dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Requerimento getRequerimento() {
        return requerimento;
    }

    public void setRequerimento(Requerimento requerimento) {
        this.requerimento = requerimento;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

}
