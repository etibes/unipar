function preencherLista(){
    document.getElementById('divAlunos').innerHTML = document.getElementById('divAlunos').innerHTML + preencherListaComAlunos()  
    /* A linha acima, está procurando a div de id = divAlunos e inserindo um código HTML dentro dela com o inner HTML. O igual é para receber o html retornado da função preencherListaComAlunos que pega os dados da variável itensDaLista */
    /* 
      Todos os elementos do HTML são manipulaveis pelo javascript através do document, que é a página propriamente dita.
    */ 
  }
  
  function preencherListaComAlunos(){
    var inicioTabela = "<table>"; //Aqui monto o cabeçalho da tabela
    var colunasTabela = "<tr> <th> Nome </th>  <th> RA </th> </tr>"; // Aqui monto as colunas
    var conteudoTabela = ""; // Inicio a variável que vai receber o conteúdo html montado com os alunos
    itensDaLista.forEach(aluno => { 
      conteudoTabela += "<tr> <td>" + aluno.nome + " </td> <td>" + "" + aluno.ra + "</td></tr>" + "\n"; //Aqui itero a lista de alunos e monto uma linha <tr> para cada aluno
    })
    var fimTabela = "</table>"; // fim da  tag de tabela
    return inicioTabela + colunasTabela + conteudoTabela + fimTabela; //Aqui será retornado o valor montado de todas as tags juntas */
  }
  
  preencherLista() // Aqui eu chamo a função, para que seja carregada, assim que o script for lido no HTML






  
  // Trata a submissão do formulário de autenticação
todoForm.onsubmit = function (event) {
    event.preventDefault() // Evita o redirecionamento da página
    if (todoForm.name.value != '') {
      var data = {
        name: todoForm.name.value
      }
  
      dbRefUsers.child(firebase.auth().currentUser.uid).push(data).then(function () {
        console.log('Tarefa "' + data.name + '" adicionada com sucesso')
      }).catch(function (error) {
        showError('Falha ao adicionar tarefa: ', error)
      })
    } else {
      alert('O nome da tarefa não pode ser em branco para criar a tarefa!')
    }
  }
  
  // Exibe a lista de tarefas do usuário
  function fillTodoList(dataSnapshot) {
    ulTodoList.innerHTML = ''
    var num = dataSnapshot.numChildren()
    todoCount.innerHTML = num + (num > 1 ? ' tarefas' : ' tarefa') + ':' // Exibe na interface o número de tarefas
    dataSnapshot.forEach(function (item) { // Percorre todos os elementos
      var value = item.val()
      var li = document.createElement('li') // Cria um elemento do tipo li
      var spanLi = document.createElement('span') // Cria um elemento do tipo span
      spanLi.appendChild(document.createTextNode(value.name)) // Adiciona o elemento de texto dentro da nossa span
      li.appendChild(spanLi) // Adiciona o span dentro do li
      ulTodoList.appendChild(li) // Adiciona o li dentro da lista de tarefas
    })
  }