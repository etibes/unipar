/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author thais
 */
public class Telefone implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Integer telefone;
    private Usuario usuario;

    public Telefone(Long id, Integer telefone, Usuario usuario) {
        this.id = id;
        this.telefone = telefone;
        this.usuario = usuario;
    }

    public Telefone() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTelefone() {
        return telefone;
    }

    public void setTelefone(Integer telefone) {
        this.telefone = telefone;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

}
