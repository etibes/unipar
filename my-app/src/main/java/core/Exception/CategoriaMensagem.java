package core.Exception;

public enum CategoriaMensagem {
    ERRO("error"), SUCESSO("success"),
    INFO("info"), AVISO("warning"),
    AJUDA("help");

    private String tipo;

    private CategoriaMensagem(String tipo) {
		this.tipo = tipo;
	}

    @Override
    public String toString() {
        return tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}